#!/usr/bin/python

# Arno van Wyk 27/10/2017
# Kerberos keytab creation tool

import subprocess
import os

buff = [] #buffer for available principals
buff_selected = [] #buffer for selected keytabs
tab_num = 0

keytab = raw_input("Please enter a filename for the keytab: ")
type(keytab)

if not os.path.exists('./keytabs'):
        os.makedirs('./keytabs')

def addprinc(primary, instance):
    print primary
    print instance
    os.system("kadmin.local -q \"addprinc -randkey "+ primary + "/" + instance + "\"")

def delprinc(name):
    os.system("kadmin.local -q \"delprinc " + name + "\"")

def listprincs():
    del buff[:] 
    index = 0
    proc = subprocess.Popen(['kadmin.local', '-q', '\"listprincs\"'],stdout=subprocess.PIPE)
    while True:
        line = proc.stdout.readline()
        if line != '':
            if line.find('Authenticating') > -1 or line.find('/') < 0 :
                continue
            buff.append(line.rstrip())
        else:
            break
    for x in buff:
        print str(index) + ": " + x
        index = index + 1
    print "\n"

def listselprincs():
    index = 0
    for x in buff_selected:
        print str(index) + ": " + x
        index = index + 1
    print "\n"

def buildkeytab():
    for x in buff_selected:
        os.system("kadmin.local -q \"ktadd -k ./keytabs/" + keytab + " " + x.split("@")[0] + "\"")

def main():
    while(1):
        os.system("clear")
        print "Current Keytab file: " + keytab + "\n"
        selection = 0
        print '\x1b[6;30;42mAVAILABLE PRINCIPALS:\x1b[0m'
        listprincs()
        print '\x1b[5;30;41mSELECTED PRINCIPALS:\x1b[0m'
        listselprincs()
        if selection == 0:
            print "\n1 - Add an existing principal to keytab"
            print "2 - Create a new principal in database"
            print "3 - Delete a principal from database"
            if len(buff_selected) != 0:
                print "4 - Write keytab file"
            selection = input("Make a selection ")
            print ""
        if selection == 1:
            tabnum = input("Select a principal to add:")
            buff_selected.append(buff[tabnum])
        if selection == 2:
            primary = raw_input("Enter a primary name(We generally use the server hostname) :")
            instance = raw_input("Enter a instance name(Server domain name):")
            addprinc(primary, instance)
        if selection == 3:
            tabnum = input("Select a principal to delete:")
            delprinc(buff[tabnum])
        if selection == 4 and len(buff_selected) != 0:
            buildkeytab()
            break

main()


